var gulp = require('gulp'),
    $ = require('gulp-load-plugins')();

gulp.task('sass', function() {
    return gulp.src('./css/style.scss')
        .pipe($.sass())
        .pipe($.minifyCss())
        .pipe(gulp.dest('./public/'));
});

gulp.task('js', function() {
    return gulp.src('./js/**/*.js')
        .pipe($.concat('app.js'))
        .pipe(gulp.dest('./public/'));
});

gulp.task('serve', $.serve({
    root: './',
    port: 3000
}));

gulp.task('watch', function() {
    gulp.watch('css/**/*.{scss,sass}', ['sass']);
    gulp.watch('js/**/*.js', ['js']);
});

gulp.task('default', ['watch', 'sass', 'js']);