angular
    .module('rssApp')
    .factory('feedLoad', feedLoad);

feedLoad.$inject = ['$http'];

function feedLoad ($http) {
    return {
        getFeeds: function(url) {
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=20&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    }
}