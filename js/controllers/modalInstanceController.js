angular
    .module('rssApp')
    .controller('modalInstanceController', modalInstanceController);

modalInstanceController.$inject = ['$scope', '$uibModalInstance', 'items'];

function modalInstanceController ($scope, $uibModalInstance, items) {

    $scope.formTitle = "Добавление новой ленты";
    $scope.formBtnTitle = "Добавить";

    $scope.feedGroups = items.feedGroups;

    if (typeof items.feed != "undefined") {
        $scope.formTitle = "Редактирование ленты";
        $scope.formBtnTitle = "Изменить";

        $scope.feed = items.feed;

        $scope.feedName = $scope.feed.name;
        $scope.feedUrl = $scope.feed.url;
    }

    if (typeof items.selectedGroupId != "undefined") {
        $scope.selectedGroupId = items.selectedGroupId;
        $scope.selectedGroup = $scope.feedGroups[$scope.selectedGroupId - 1];
    } else {
        $scope.selectedGroup = $scope.feedGroups[0];
    }

    $scope.ok = function () {
        $scope.selected = {
            feedName: $scope.feedName,
            feedUrl: $scope.feedUrl,
            groupId: $scope.selectedGroup.id
        }

        $uibModalInstance.close($scope.selected);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}