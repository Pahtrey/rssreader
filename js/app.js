$(document).ready(function() {
    $('.sidebar').mCustomScrollbar();
    $('.content-sidebar').mCustomScrollbar();
    $('.content-feeds').mCustomScrollbar();
});

angular
    .module('rssApp', ['ui.bootstrap', 'ngSanitize']);
