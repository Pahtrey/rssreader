angular
    .module('rssApp')
    .directive('ngEnter', ngEnter);

ngEnter.$inject = ['$timeout'];

function ngEnter ($timeout) {
    return function ($scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if (event.which === 13) {
                $timeout(function () {
                    $scope.$eval(attrs.ngEnter);
                }, 0);

                event.preventDefault();
            }
        });
    }
}