$(document).ready(function() {
    $('.sidebar').mCustomScrollbar();
    $('.content-sidebar').mCustomScrollbar();
    $('.content-feeds').mCustomScrollbar();
});

angular
    .module('rssApp', ['ui.bootstrap', 'ngSanitize']);

angular
    .module('rssApp')
    .controller('modalInstanceController', modalInstanceController);

modalInstanceController.$inject = ['$scope', '$uibModalInstance', 'items'];

function modalInstanceController ($scope, $uibModalInstance, items) {

    $scope.formTitle = "Добавление новой ленты";
    $scope.formBtnTitle = "Добавить";

    $scope.feedGroups = items.feedGroups;

    if (typeof items.feed != "undefined") {
        $scope.formTitle = "Редактирование ленты";
        $scope.formBtnTitle = "Изменить";

        $scope.feed = items.feed;

        $scope.feedName = $scope.feed.name;
        $scope.feedUrl = $scope.feed.url;
    }

    if (typeof items.selectedGroupId != "undefined") {
        $scope.selectedGroupId = items.selectedGroupId;
        $scope.selectedGroup = $scope.feedGroups[$scope.selectedGroupId - 1];
    } else {
        $scope.selectedGroup = $scope.feedGroups[0];
    }

    $scope.ok = function () {
        $scope.selected = {
            feedName: $scope.feedName,
            feedUrl: $scope.feedUrl,
            groupId: $scope.selectedGroup.id
        }

        $uibModalInstance.close($scope.selected);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}
angular
    .module('rssApp')
    .controller('rssController', rssController);

rssController.$inject = ['$scope', '$uibModal', '$window', 'feedLoad', '$sce', '$timeout'];

function rssController ($scope, $uibModal, $window, feedLoad, $sce, $timeout) {

    var rssData = JSON.parse(localStorage.getItem('rssData'));

    rssData = (rssData !== null) ? rssData: {};
    $scope.feedGroups = (typeof rssData.groups !== 'undefined') ? rssData.groups : [];
    $scope.readedFeeds = (typeof rssData.readed !== 'undefined') ? rssData.readed : [];

    $scope.$on('load', function () {
        $scope.preloader = true;
    });

    $scope.$on('unload', function () {
        $scope.preloader = false;
    });

    $scope.displayError = false;

    $scope.groupColor = {
        1: 'red',
        2: 'orange',
        3: 'yellow',
        4: 'green',
        5: 'cyan',
        6: 'blue',
        7: 'purple'
    };

    $scope.defaultColor = $scope.groupColor[1];

    $scope.feedListEmptyMessage = "Список лент пуст";
    $scope.feedEmptyMessage = "Список новостей пуст";

    $scope.setColor = function (colorId) {
        $scope.defaultColor = $scope.groupColor[colorId];
    };

    $scope.setFeedGroup = function () {
        var feedGroupLenght = $scope.feedGroups.length;
        var groupId = feedGroupLenght > 0 ? $scope.feedGroups[feedGroupLenght - 1].id + 1 : 1;

        $scope.feedGroups.push({
            id: groupId,
            name: $scope.groupName,
            color: $scope.defaultColor,
            feed: []
        });

        $scope.groupName = null;
        $scope.defaultColor = $scope.groupColor[1];
    };

    $scope.dropFeedGroup = function (groupId) {
        if ($scope.getFeedGroupId(groupId).feed.length == 0) {
            $scope.feedGroups.splice($scope.feedGroups.indexOf($scope.getFeedGroupId(groupId)), 1);
        } else {
            $scope.error = 'Группа лент не пустая';
            $scope.displayError = true;

            $timeout(function() {
                $scope.displayError = false;
            }, 2000);
        }
    };

    $scope.showFeedList = function (groupId) {
        $scope.feedList = {};
        $scope.feeds = [];
        $scope.feedListEmptyMessage = "";

        if ($scope.getFeedGroupId(groupId).feed.length > 0) {
            $scope.feedList = {
                'groupId': groupId,
                'feed': $scope.getFeedGroupId(groupId).feed
            };
        } else {
            $scope.feedListEmptyMessage = "Список лент пуст";
        }

        $scope.activeFeedGroup = $scope.getFeedGroupId(groupId).id;
    };

    $scope.isActiveFeedGroup = function (id) {
        return id == $scope.activeFeedGroup;
    };

    $scope.dropFeedList = function (groupId, index) {
        if (index == $scope.activeFeedList) {
            $scope.activeFeedList = null;
            $scope.feeds = [];
        }
        $scope.getFeedGroupId(groupId).feed.splice(index, 1);
    };

    $scope.getFeedGroupId = function (groupId) {
        var feedGroupsList = $scope.feedGroups;
        for (var i = 0; i < $scope.feedGroups.length; i++) {
            if (feedGroupsList[i].id == groupId) {
                return feedGroupsList[i];
            }
        }
    };

    $scope.loadRss = function (url, index) {
        $scope.activeFeedList = index;

        $scope.$emit('load');

        feedLoad.getFeeds(url).then(function (res) {
            $scope.$emit('unload');
            $scope.feedEmptyMessage = '';
            if (res.data.responseData != null) {
                $scope.feeds = res.data.responseData.feed.entries;

                for (var i = 0; i < $scope.feeds.length; i++) {
                    $scope.feeds[i].datePub = new Date($scope.feeds[i].publishedDate);
                    $scope.feeds[i].showDescriptionText = 'Читать описание';
                    if ($scope.readedFeeds.indexOf($scope.feeds[i].link) > -1) {
                        $scope.feeds[i].readed = "Прочитано";
                    } else {
                        $scope.feeds[i].readed = "";
                    }
                }
            } else {
                $scope.feedEmptyMessage = 'Список новостей пуст';
                $scope.feeds = [];
            }
        });
    };

    $scope.renderHtml = function (html) {
        return $sce.trustAsHtml(html);
    };

    $scope.showDescription = function (feed, index) {
        var activeClass = 'active';

        if ($scope.readedFeeds.indexOf(feed.link) == -1) {
            $scope.readedFeeds.push(feed.link);
            $scope.feeds[index].readed = "Прочитано";
        }

        if (feed.active != activeClass) {
            feed.active = activeClass;
            $scope.feeds[index].showDescriptionText = "Скрыть описание";
        }
        else {
            feed.active = '';
            $scope.feeds[index].showDescriptionText = "Читать описание";
        }
    };

    $scope.getIconByUrl = function (url) {
        var iconSrc = '',
            parseUrl = url.match('^(http|https):\/\/([A-Za-z0-9_]*)(\.)([A-Za-z0-9_]*)\/');

        if (parseUrl !== null) {
            iconSrc = parseUrl[0] + 'favicon.ico';
        }

        return iconSrc;
    };

    $window.onunload = function () {
        var rssData = {
            'groups': $scope.feedGroups,
            'readed': $scope.readedFeeds
        }

        localStorage.setItem('rssData', JSON.stringify(rssData));
    };

    $scope.openModal = function (groupId, feed, index) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            controller: 'modalInstanceController',
            templateUrl: 'modalForm.html',
            resolve: {
                items: function () {
                    var feedGroups = [];

                    angular.forEach($scope.feedGroups, function (value, key) {
                        feedGroups.push({
                            id: value.id,
                            name: value.name
                        });
                    });

                    return {
                        "feed": feed,
                        "selectedGroupId": groupId,
                        "feedGroups": feedGroups
                    };
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;

             if (typeof groupId == "undefined") {
                 $scope.getFeedGroupId($scope.selected.groupId).feed.push({
                     'name': $scope.selected.feedName,
                     'url': $scope.selected.feedUrl
                 });
             } else {
                 if ($scope.selected.groupId != groupId) {
                     $scope.getFeedGroupId(groupId).feed.splice(index, 1);
                     $scope.getFeedGroupId($scope.selected.groupId).feed.push({
                         'name': $scope.selected.feedName,
                         'url': $scope.selected.feedUrl
                     });
                 } else {
                     $scope.getFeedGroupId(groupId).feed[index] = {
                         'name': $scope.selected.feedName,
                         'url': $scope.selected.feedUrl
                     }
                 }
             }
        });
    }
}
angular
    .module('rssApp')
    .factory('feedLoad', feedLoad);

feedLoad.$inject = ['$http'];

function feedLoad ($http) {
    return {
        getFeeds: function(url) {
            return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=20&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
        }
    }
}
angular
    .module('rssApp')
    .directive('ngEnter', ngEnter);

ngEnter.$inject = ['$timeout'];

function ngEnter ($timeout) {
    return function ($scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if (event.which === 13) {
                $timeout(function () {
                    $scope.$eval(attrs.ngEnter);
                }, 0);

                event.preventDefault();
            }
        });
    }
}